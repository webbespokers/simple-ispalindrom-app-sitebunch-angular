export const isPalindrome = (inputText: string) => {
  const length = inputText.length;
  for (let i = 0; i < length / 2; i++) {
    if (inputText[i] !==  inputText[length - i - 1]) {
      return false;
    }
  }
  return true;
};
