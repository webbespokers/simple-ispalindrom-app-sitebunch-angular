import { isPalindrome} from './isPalindrome';

describe('Utils', () => {
  it('isPalindrome', () => {
    expect(isPalindrome('kajak')).toBeTrue();
    expect(isPalindrome('02022020')).toBeTrue();
    expect(isPalindrome('palindrome')).toBeFalse();
  });

});
